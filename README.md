# TLDR; #
Be aware that all of these use secrets.json, so if you use multiple, it makes sense to only push once.

# Quicksilver Pushback - DOES NOT WORK #

This Quicksilver project is used in conjunction with the various suite of [Terminus Build Tools](https://github.com/pantheon-systems/terminus-build-tools-plugin)-based example repositories to push any commits made on the Pantheon dashboard back to the original Git repository for the site. This allows developers (or other users) to work on the Pantheon dashboard in SFTP mode and commit their code, through Pantheon, back to the canonical upstream repository via a PR. This is especially useful in scenarios where you want to export configuration (Drupal, WP-CFM).

This project is maintained in it's own repo located at https://github.com/pantheon-systems/quicksilver-pushback. Check that page for more information about the project, including installation instructions. Please note that it comes installed automatically if you use the Terminus Build Tools plugin, so you probably don't need to install it yourself unless you're following a non-standard workflow.

# Asana Integration - DOES NOT WORK #

This parses commit messages for Asana task IDs and adds the commit message as a comment in the related Asana task.

Example comments:

  [389749465118801]: Adjust layout spacing

Commits that contain multiple Asana tasks will post comments to each issue mentioned. A comment will be added each time a commit is pushed to any dev or multidev branch; each Asana comment is labeled with the appropriate commit hash and Pantheon environment that triggered the post.

## Instructions ##

- In Asana, go to My Profile Settings -> Apps -> Manage Developer Apps -> Create New Personal Access Token and copy the new token.
- Store the Asana Personal Access Token into a file called `secrets.json` and store it in the private files area of your site

```shell
  $> echo '{"asana_access_token" : "Your generated Personal Access Token" }' > secrets.json
  # Note, you'll need to copy the secrets into each environment where you want to save commit messages to Asana
  $> `terminus site connection-info --env=dev --site=your-site --field=sftp_command`
      Connected to appserver.dev.d1ef01f8-364c-4b91-a8e4-f2a46f14237e.drush.in.
  sftp> cd files
  sftp> mkdir private
  sftp> cd private
  sftp> put secrets.json

```
- Add the example `asana_integration.php` script to the `private` directory of your code repository.
- Add a Quicksilver operation to your `pantheon.yml` to fire the script after a deploy.
- Push code with a commit message containing an Asana task ID!

Note: If you open the task in Asana the URL will look something like this: https://app.asana.com/0/389749465118800/389749465118801
The second number is your task ID. Surround it with [] in your commits.

Optionally, you may want to use the `terminus workflow:watch` command to get immediate debugging feedback.

### Example `pantheon.yml` ###

Here's an example of what your `pantheon.yml` would look like if this were the only Quicksilver operation you wanted to use:

```yaml
api_version: 1

workflows:
  sync_code:
    after:
      - type: webphp
        description: Asana Integration
        script: private/scripts/quicksilver/jd-quicksilvers/asana_integration.php
```
# Quicksilver Debugging #

This example is intended for users who want to explore the potential for Quicksilver with a quick debugging example. 

Setting up this example is easy:

1. Add the example `debug.php` script to the `private` directory of your code repository.
2. Add a Quicksilver operation to your `pantheon.yml` to fire the script after cache clears.
3. Fire up terminus to watch the workflow log.
4. Push everything to Pantheon.
5. Clear the caches and see the output!

### Example `pantheon.yml` ###

Here's an example of what your `pantheon.yml` would look like if this were the only Quicksilver operation you wanted to use:

```yaml
api_version: 1

workflows:
  clear_cache:
    after:
      - type: webphp
        description: Dump debugging output
        script: private/scripts/quicksilver/jd-quicksilvers/debug.php
```

### Example `terminus workflow:watch` Output ###

Triggering cache clears from your dashboard you should enjoy nice debugging output like this:

```shell
$> terminus workflow:watch your-site-name
[2015-12-15 03:17:26] [info] Watching workflows...
[2015-12-15 03:17:50] [info] Started 1c5421b8-a2db-11e5-8a28-bc764e10b0ce Clear cache for "dev" (dev)
[2015-12-15 03:17:58] [info] Finished Workflow 1c5421b8-a2db-11e5-8a28-bc764e10b0ce Clear cache for "dev" (dev)
[2015-12-15 03:18:00] [info]
------ Operation: Dump debugging output finished in 2s ------
Quicksilver Debuging Output


========= START PAYLOAD ===========
Array
(
    [wf_type] => clear_cache
    [user_id] => ed828d9d-2389-4e8d-9f71-bd2fcafc93c2
    [site_id] => 6c5ee454-9427-4cce-8193-a44d6c54172c
    [user_role] => owner
    [trace_id] => 1c4b90c0-a2db-11e5-9ca4-efb1318547fc
    [environment] => dev
    [wf_description] => Clear cache for "dev"
    [user_email] => josh@getpantheon.com
)

========== END PAYLOAD ============
```

The `wf_type`, `wf_description` and `user_email` values are likely to be of particular interest. You can get additional information from the `$_SERVER` and `$_ENV` superglobals. You can also interrogate the status of the git repository, as well as bootstrapping the CMS. There are lots of possibilities! Have fun with Quicksilver!

# Visual Regression Testing via Diffy.website #

This example will show you how to integrate [Diffy.website](http://Diffy.website)'s visual regression operation into your deployment workflow.

This will allow you to do a comparative visual diff between the live environment and the test environemnt everytime you deploy to the testing environment.

For more advanced use cases, including doing visual regression against Multidev instances, this script can be easily adapted for Diffy.website's REST API: [https://diffy.website/rest](https://diffy.website/rest).

## Instructions ##

Vide demo is available https://youtu.be/U8uHJELeTDE.

In order to get up and running, you first need to setup a Diffy.website project:

1. Either login to your account or register for a new one at [https://diffy.website](https://diffy.website).
2. Setup a Diffy project for your site and define the Production and Staging URLs in the project settings.

Then you need to add the relevant code to your Pantheon project:

1. Add the example `diffyVisualregression.php` script to the 'private/scripts/quicksilver/jd-quicksilvers/' directory of your code repository.
2. Create an API token in Diffy (). Copy the token and project_id into a file called `secrets.json` and store it in the [private files](https://pantheon.io/docs/articles/sites/private-files/) directory.

     ```shell
       $> echo '{"token": "yourToken", "project_id" : "123"}' > secrets.json
       sftp YOURCREDENTIALS_TO_LIVE_ENVIRONMENT
       sftp> cd files
       sftp> mkdir private
       sftp> cd private
       sftp> put secrets.json
       sftp> quit
       ```

3. Add a Quicksilver operation to your `pantheon.yml` to fire the script after a deploy to test.
```
api_version: 1

workflows:
  deploy:
    after:
      - type: webphp
        description: Do a visual regression test with Diffy.website
        script: private/scripts/quicksilver/jd-quicksilvers/diffyVisualregression.php
```
4. Make a deploy to test environment!

Optionally, you may want to use the `terminus workflows watch YOUR_SITE_ID` command to get immediate debugging feedback. First you would need to install and authenticate your terminus.

# New Relic Deploy Logs #

This example will show you how you can automatically log changes to your site into [New Relic's Deployments Page](https://docs.newrelic.com/docs/apm/applications-menu/events/deployments-page) when the workflow fires on Pantheon. This can be quite useful for keeping track of all your performance improvements!

This script uses a couple clever tricks to get data about the platform. First of all it uses the `pantheon_curl()` command to fetch the extended metadata information for the site/environment, which includes the New Relic API key. It also uses data within the git repository on the platform to pull out deploy tag numbers and log messages. 

> **Note:** This example will work for all Pantheon sites once the bundled [New Relic APM Pro feature](https://pantheon.io/features/new-relic) is activated, regardless of service level. 

## Instructions ##

Setting up this example is easy:

1. [Activate New Relic Pro](https://pantheon.io/docs/new-relic/#activate-new-relic-pro) within your site Dashboard. 
2. Add the example `new_relic_deploy.php` script to the `private` directory of your code repository.
3. Add a Quicksilver operation to your `pantheon.yml` to fire the script after a deploy.
4. Test a deploy out!

Optionally, you may want to use the `terminus workflow:watch yoursitename` command to get immediate debugging feedback.

### Example `pantheon.yml` ###

Here's an example of what your `pantheon.yml` would look like if this were the only Quicksilver operation you wanted to use:

```yaml
# Always need to specify the pantheon.yml API version.
api_version: 1

# You might also want some of the following here:
# php_version: 7.0
# drush_version: 8

workflows:
  # Log to New Relic when deploying to test or live.
  deploy:
    after:
      - type: webphp
        description: Log to New Relic
        script: private/scripts/quicksilver/jd-quicksilvers/new_relic_deploy.php
  # Also log sync_code so you can track new code going into dev/multidev.
  sync_code:
    after:
      - type: webphp
        description: Log to New Relic
        script: private/scripts/quicksilver/jd-quicksilvers/new_relic_deploy.php

```

# Slack Integration #

This script shows how easy it is to integrate Slack notifications from your Pantheon project using Quicksilver. As a bonus, we also show you how to manage API keys outside of your site repository.

## Instructions ##

1. [Enable Incoming Webhooks](https://my.slack.com/services/new/incoming-webhook/) for your Slack instance.
2. Copy the secret Webhook URL into a file called `secrets.json` and store it in the [private files](https://pantheon.io/docs/articles/sites/private-files/) directory of every environment where you want to trigger Slack notifications.

  ```shell
    $> echo '{"slack_url": "https://hooks.slack.com/services/MY/SECRET/URL"}' > secrets.json
    # Note, you'll need to copy the secrets into each environment where you want to trigger Slack notifications.
    $> `terminus connection:info  --field=sftp_command site.env`
        Connected to appserver.dev.d1ef01f8-364c-4b91-a8e4-f2a46f14237e.drush.in.
    sftp> cd files  
    sftp> mkdir private
    sftp> cd private
    sftp> put secrets.json
    sftp> quit
  ```

3. Add, and update as needed, the example `slack_notification.php` script to the `private` directory in the root of your site's codebase, that is under version control. Note this is a different `private` directory than where the secrets.json is stored.
4. Add Quicksilver operations to your `pantheon.yml`
5. Test a deploy out!

Optionally, you may want to use the `terminus workflows watch` command to get immediate debugging feedback. You may also want to customize your notifications further. The [Slack API](https://api.slack.com/incoming-webhooks) documentation has more on your options.

### Example `pantheon.yml` ###

Here's an example of what your `pantheon.yml` would look like if this were the only Quicksilver operation you wanted to use.  Pick and choose the exact workflows that you would like to see notifications for.

```yaml
api_version: 1

workflows:
  deploy_product:
    after:
        - type: webphp
          description: Post to Slack after site creation
          script: private/scripts/quicksilver/jd-quicksilvers/slack_notification.php
  create_cloud_development_environment:
    after: 
        - type: webphp
          description: Post to Slack after Multidev creation
          script: private/scripts/quicksilver/jd-quicksilvers/slack_notification.php
  deploy:
    after:
        - type: webphp
          description: Post to Slack after deploy
          script: private/scripts/quicksilver/jd-quicksilvers/slack_notification.php
  sync_code:
    after:
        - type: webphp
          description: Post to Slack after code commit
          script: private/scripts/quicksilver/jd-quicksilvers/slack_notification.php
  clear_cache:
    after:
        - type: webphp
          description: Someone is clearing the cache again
          script: private/scripts/quicksilver/jd-quicksilvers/slack_notification.php
```

# Trello Integration #

This example parses commit messages for Trello card IDs and adds the commit message as a comment in the related Trello card.

Example comments:

  [s3yxNR5v]: Adjust layout spacing

Commits that contain multiple Trello cards will post comments to each issue mentioned. A comment will be added each time a commit is pushed to any dev or multidev branch; each Trello comment is labeled with the appropriate commit hash and Pantheon environment that triggered the post.

## Instructions ##

- Go to https://trello.com/app-key and copy your app key. Also click the link to generate a token for yourself, approve access and copy the token.
- Copy your Trello credentials (key + token) into a file called `secrets.json` and store it in the private files area of your site

```shell
  $> echo '{"trello_key" : "Your App Key" , "trello_token" : "Your generated token" }' > secrets.json
  # Note, you'll need to copy the secrets into each environment where you want to save commit messages to Trello
  $> `terminus site connection-info --env=dev --site=your-site --field=sftp_command`
      Connected to appserver.dev.d1ef01f8-364c-4b91-a8e4-f2a46f14237e.drush.in.
  sftp> cd files
  sftp> mkdir private
  sftp> cd private
  sftp> put secrets.json

```
- Add the example `trello_integration.php` script to the `private` directory of your code repository.
- Add a Quicksilver operation to your `pantheon.yml` to fire the script after a deploy.
- Push code with a commit message containing a Trello card ID!

Optionally, you may want to use the `terminus workflow:watch` command to get immediate debugging feedback.

### Example `pantheon.yml` ###

Here's an example of what your `pantheon.yml` would look like if this were the only Quicksilver operation you wanted to use:

```yaml
api_version: 1

workflows:
  sync_code:
    after:
      - type: webphp
        description: Trello Integration
        script: private/scripts/quicksilver/jd-quicksilvers/trello_integration.php
```
